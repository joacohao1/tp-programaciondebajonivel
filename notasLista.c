#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct structNotas
{
    char *nombreEstudianteDeLaNota;
    char *nombreMateria;
    int notaParcial1;
    int notaParcial2;
    int notaFinal;
    struct structNotas *siguiente;
} Nota;

Nota *crearListaNotas()
{
    Nota *nuevaListaNotas = NULL;
    return nuevaListaNotas;
}
Nota *agregarNota(Nota *nuevaListaNotas, const char *nombreEstudiante, const char *nombreMateria, int parcial1, int parcial2, int final)
{
    Nota *nodoNuevoNota = malloc(sizeof(Nota));
    nodoNuevoNota->nombreEstudianteDeLaNota = malloc(strlen(nombreEstudiante) + 1);
    strcpy(nodoNuevoNota->nombreEstudianteDeLaNota, nombreEstudiante);

    nodoNuevoNota->nombreMateria = malloc(strlen(nombreMateria) + 1);
    strcpy(nodoNuevoNota->nombreMateria, nombreMateria);

    nodoNuevoNota->notaParcial1 = parcial1;
    nodoNuevoNota->notaParcial2 = parcial2;
    nodoNuevoNota->notaFinal = final;
    nodoNuevoNota->siguiente = NULL;
    if (nuevaListaNotas == NULL) // si la lista está vacia
    {
        nuevaListaNotas = nodoNuevoNota;
    }
    else // si ya hay un elemento en la lista
    {
        Nota *cursor = nuevaListaNotas;
        while (cursor->siguiente != NULL)
        {
            cursor = cursor->siguiente;
        }
        cursor->siguiente = nodoNuevoNota;
    }
    return nuevaListaNotas;
}

void showNotas(Nota *nuevaListaNotas)
{
    Nota *nota = nuevaListaNotas;
    int contador = 0;
    
    for (nota = nuevaListaNotas; nota != NULL; nota = nota->siguiente)
    {
        contador++;
        printf("\n(%d)Estudiante:%s  |  Materia: %s\n", contador, nota->nombreEstudianteDeLaNota, nota->nombreMateria);
        printf("Parcial1 : %d  |  Parcial 2: %d  |  Final: %d \n\n", nota->notaParcial1, nota->notaParcial2, nota->notaFinal);
    }
}