#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listaEstudiantes.c"

int main()
{
    int opcion;
    Estudiante *listaEstudiantes = crearListaEstudiantes();
    Materia *listaMaterias = crearListaMaterias();
    Nota *listaNotas = crearListaNotas();
    Estudiante *listaDeLegajos = crearListaEstudiantes();

    do
    {
        printf("------------MENU------------\n");
        printf("1. Agregar Estudiante \n");
        printf("2. Listar Estudiantes \n");
        printf("3. Buscar Estudiante por Nombre \n");
        printf("4. Listar Estudiantes por Rango de Edad \n");
        printf("5. Agregar Materia \n");
        printf("6. Listar Materias \n");
        printf("7. Anotar en una Materia \n");
        printf("8. Rendir Materia \n");
        printf("9. Remover Estudiante/s \n");
        printf("10. Remover Materia/s \n");
        printf("11. Carga Automatica de Estudiantes \n");
        printf("12. Carga Automatica de Materias \n");
        printf("13. Imprimir notas\n");
        printf("14. Buscar nota de un estudiante \n");
        printf("15. Lista de legajos ordenados\n");
        // solo para agregados de manera manual

        printf("16. Salir \n");
        printf("----------------------------\n");
        scanf("%d", &opcion);
        switch (opcion)
        {

        case 1:
            printf("Agregar Estudiante: \n");
            int salida1 = -1;
            while (salida1 != 0)
            {

                char nombre[100];
                int edad;
                int legajo; //= rand() % 9999;

                printf("Ingrese el nombre (ingrese 0 para volver al menu): \n");
                scanf(" %[^\n]", &nombre);
                if (*nombre == '0')
                {
                    break;
                }
                printf("Ingrese la edad (ingrese 0 para volver al menu): ");
                scanf("%d", &edad);
                if (edad == 0)
                {
                    break;
                }
                printf("Ingrese la legajo (ingrese 0 para volver al menu): ");
                scanf("%d", &legajo);
                if (legajo == 0)
                {
                    break;
                }
                listaEstudiantes = agregarEstudiante(listaEstudiantes, nombre, edad, legajo);
                listaDeLegajos = agregarPorLegajo(listaDeLegajos, nombre, edad, legajo);
                printf("Se agrego el estudiante\n");
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida1);
            }
            break;

        case 2:
            printf("Elegiste listar estudiantes\n");
            int salida2 = -1;
            while (salida2 != 0)
            {
                printf("Listar Estudiantes: \n");

                showEstudiantes(listaEstudiantes);
                printf("Ingrese 0 para volver al menu\n");
                scanf("%d", &salida2);
            }
            break;

        case 3:
            printf("Buscar estudiante por nombre\n");
            int salida3 = -1;
            while (salida3 != 0)
            {
                printf("Buscar Estudiante por Nombre: \n");

                char nombreBuscado[100];

                printf("Ingrese el nombre (ingrese 0 para volver al menu): ");
                scanf(" %[^\n]", nombreBuscado);
                if (*nombreBuscado == '0')
                {
                    break;
                }
                Estudiante *resultado = buscarPorNombre(listaEstudiantes, nombreBuscado);
                if (resultado != NULL)
                {
                    printf("Se encontro al estudiante %s\n", resultado->nombreEstudiante);
                    printf("Edad: %d\t| Legajo: %d\n", resultado->edad, resultado->legajo);
                }
                else
                {
                    printf("No se encontro al estudiante\n");
                }

                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida3);
            }
            break;

        case 4:
            printf("Listar por rango de edad\n");
            int salida4 = -1;
            Estudiante *encontrados;
            while (salida4 != 0)
            {
                int edadMin;
                int edadMax;
                printf("Ingrese una Rango de edad minima (ingrese 0 para volver al menu): \n");
                scanf("%d", &edadMin);
                if (edadMin == 0)
                {
                    break;
                }
                printf("Ingrese una Rango de edad maxima (ingrese 0 para volver al menu): \n");
                scanf("%d", &edadMax);
                if (edadMax == 0)
                {
                    break;
                }
                encontrados = buscarPorRangoDeEdad(listaEstudiantes, edadMin, edadMax);
                if (encontrados != NULL)
                {
                    showEstudiantes(encontrados);
                }
                else
                {
                    printf("No se encontro ningun estudiante en el rango\n");
                }
                //
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida4);
            }
            break;

        case 5:
            printf("Agregar materia\n");
            int salida5 = -1;
            while (salida5 != 0)
            {
                printf("Agregar Materia: \n");
                char nombreMateria[100];
                int identificador;

                printf("Ingrese el nombre de la materia (ingrese 0 para volver al menu): ");
                scanf(" %[^\n]", nombreMateria);
                if (*nombreMateria == '0')
                {
                    break;
                }
                printf("Ingrese el identificador de la materia (ingrese 0 para volver al menu): ");
                scanf("%d", &identificador);
                if (identificador == 0)
                {
                    break;
                }
                listaMaterias = agregarMateria(listaMaterias, nombreMateria, identificador);
                printf("Se agrego la materia\n");
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida5);
            }
            break;

        case 6:
            printf("Listar materias\n");
            int salida6 = -1;
            while (salida6 != 0)
            {
                printf("Listar Materias: \n");
                //
                showMaterias(listaMaterias);
                //
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida6);
            }
            break;

        case 7:
            printf("Anotarse a una materia\n");
            int salida7 = -1;
            while (salida7 != 0)
            {
                printf("Anotar en una Materia: \n");
                char nombreBuscados[100];
                char materiaBuscadas[100];
                printf("Ingrese el estudiante (ingrese 0 para volver al menu):");
                scanf(" %[^\n]", nombreBuscados);
                if (*nombreBuscados == '0')
                {
                    break;
                }
                printf("Ingrese la materia (ingrese 0 para volver al menu):");
                scanf(" %[^\n]", materiaBuscadas);
                if (*materiaBuscadas == '0')
                {
                    break;
                }
                //
                anotar(listaEstudiantes, listaMaterias, nombreBuscados, materiaBuscadas);
                //
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida7);
            }
            break;

        case 8:
            printf("Rendir materia\n");
            int salida8 = -1;
            while (salida8 != 0)
            {
                printf("Rendir Materia: \n");
                char estudianteNombre[100];
                char materiaNombre[100];
                int notaParcial1;
                int notaParcial2;
                int notaFinal;
                printf("Ingrese el nombre del estudiante a calificar (ingrese 0 para volver al menu):");
                scanf(" %[^\n]", estudianteNombre);
                
                
                if (*estudianteNombre == '0')
                {
                    break;
                }
                printf("Ingrese materia (ingrese 0 para volver al menu):");
                scanf(" %[^\n]", &materiaNombre);
                
                if (*materiaNombre == '0')
                {
                    break;
                }
               
                printf("Ingrese la nota del 1er parcial:");
                scanf("%d", &notaParcial1);
                printf("Ingrese la nota del 2do parcial:");
                scanf("%d", &notaParcial2);
                printf("Ingrese la nota de la evaluacion final:");
                scanf("%d", &notaFinal);

                // si var = 1, entonces el estudiante existe y esta anotado a la materia
                int var = verificarMateriaEstudiante(listaNotas, listaMaterias, listaEstudiantes, estudianteNombre, materiaNombre, notaParcial1, notaParcial2, notaFinal);
                if (var == 1)
                {
                    listaNotas = agregarNota(listaNotas, estudianteNombre, materiaNombre, notaParcial1, notaParcial2, notaFinal);
                    printf("Se agrego la nota\n");
                }
                else
                {
                    printf("No se encontro el estudiante o la materia\n");
                }

                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida8);
            }
            break;

        case 9:
            printf("Remover estudiante\n");
            int salida9 = -1;
            int opciones;
            while (salida9 != 0)
            {
                printf("Desea remover un estudiante o todos? \n");
                printf("Presione 1 para eliminar un estudiante por nombre\n");
                printf("Presione 2 para eliminar toda la lista\n");
                printf("Presione 0 para volver\n");

                scanf("%d", &opciones);
                if (opciones == 1)
                {

                    printf("Ingrese el nombre del estudiante (ingrese 0 para volver al menu): ");
                    char buscado[50];
                    scanf(" %[^\n]", buscado);
                    if (*buscado == '0')
                    {
                        break;
                    }
                    Estudiante *resultado = buscarPorNombre(listaEstudiantes, buscado);
                    if (resultado != NULL)
                    {
                        removerEstudiante(&listaEstudiantes, buscado);
                        printf("Se elimino el estudiante\n");
                    }
                    else
                    {
                        printf("No se encontro el estudiante buscado\n");
                    }
                }
                else if (opciones == 2)
                {
                    int opcionFinal;
                    printf("Estas seguro? Se va a eliminar toda la lista \n");
                    printf("Presiona 1 para continuar\n");
                    printf("Presiona 0 para regresar\n");
                    scanf("%d", &opcionFinal);

                    if (opcionFinal == 1)
                    {
                        removerTodos(&listaEstudiantes);
                        printf("Se ha eliminado la lista\n");
                    }
                    else if (opcionFinal == 0)
                    {
                        break;
                    }
                }
                else if (opciones == 0)
                {
                    break;
                }
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida9);
            }
            break;

        case 10:
            printf("Remover Materia: \n");
            int salida10 = -1;
            int opcionesMaterias;
            while (salida10 != 0)
            {
                printf("Desea remover una materia o todas? \n");
                printf("Presione 1 para eliminar una materia por nombre\n");
                printf("Presione 2 para eliminar toda la lista\n");
                printf("Presione 0 para volver\n");
                scanf("%d", &opcionesMaterias);
                if (opcionesMaterias == 1)
                {

                    char nombreMateria[100];
                    printf("Indique nombre de materia a eliminar (ingrese 0 para volver al menu):");
                    scanf(" %[^\n]", nombreMateria);
                    if (*nombreMateria == '0')
                    {
                        break;
                    }
                    Materia *buscada = buscarMateria(listaMaterias, nombreMateria);
                    if (buscada != NULL)
                    {
                        removerMateria(&listaMaterias, nombreMateria);
                        printf("Se removio la materia\n");
                    }
                    else
                    {
                        printf("No se encontro la materia buscada\n");
                    }
                }else if(opcionesMaterias== 2){
                     int opcionFinalMaterias;
                    printf("Estas seguro? Se va a eliminar toda la lista \n");
                    printf("Presiona 1 para continuar\n");
                    printf("Presiona 0 para regresar\n");
                    scanf("%d", &opcionFinalMaterias);

                    if (opcionFinalMaterias == 1)
                    {
                        removerTodas(&listaMaterias);
                        printf("Se ha eliminado la lista\n");
                    }
                    else if (opcionFinalMaterias == 0)
                    {
                        break;
                    }
                }else if(opcionesMaterias == 0){
                    break;
                }

                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida10);
            }
            break;

        case 11:

            printf("Carga Automatica Estudiantes:\n");
            int salida11 = -1;
            while (salida11 != 0)
            {
                printf("Carga Automatica de Estudiantes: \n");
                //
                int cant; //= rand() % 9999;

                printf("Ingrese cantidad a generar (ingrese 0 para volver al menu): ");
                scanf("%d", &cant);
                if (cant == 0)
                {
                    break;
                }

                listaEstudiantes = generarEstudiantes(listaEstudiantes, cant);

                printf("\n\n");
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida11);
            }
            break;

        case 12:

            printf("Carga Automatica Materias:\n");
            int salida12 = -1;
            while (salida12 != 0)
            {
                printf("Carga Automatica de Materias: \n");
                int cantMaterias; //= rand() % 9999;

                printf("Ingrese cantidad a generar (ingrese 0 para volver al menu): ");
                scanf("%d", &cantMaterias);
                if (cantMaterias == 0)
                {
                    break;
                }
                listaMaterias = generarMaterias(listaMaterias, cantMaterias);
                printf("\n\n");
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida12);
            }
            break;

        case 13:
            printf("13. Imprimir notas\n");
            int salida13 = -1;
            while (salida13 != 0)
            {
                
                showNotas(listaNotas);
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida13);
            }
            break;

        case 14:
            printf("Buscar nota de un estudiante \n");
            int salida14 = -1;
            while (salida14 != 0)
            {

                char nombreEstudiante[100];
                printf("Ingrese el nombre del estudiante (0 para volver al menu):");
                scanf(" %[^\n]", nombreEstudiante);
                if (*nombreEstudiante == '0')
                {
                    break;
                }
                showNotasPorEstudiante(listaNotas, listaEstudiantes, nombreEstudiante);
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida14);
            }

            break;

        case 15:
            printf("Ordenados ascendentemente por legajo\n");
            int salida15 = -1;
            while (salida15 != 0)
            {
                showEstudiantes(listaDeLegajos);
                printf("Presione 0 para volver al menu\n");
                scanf("%d", &salida15);
            }
            break;

        case 16:
            printf("Has salido\n");
            break;

        default:
            printf("Opcion no encontrada \n");
            printf("\n \n");
        }
    } while (opcion != 16);
    return 0;
}
