#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "listaMaterias.c"
#include "notasLista.c"

typedef struct structEstudiantes
{
    char *nombreEstudiante;
    int edad;
    int legajo;
    struct structEstudiantes *siguiente;
    struct structMaterias *materiasInscriptas;
} Estudiante;

// Contador para saber cuantos estudiantes hay:

int contadorEstudiantes = 0;

// Crear lista de estudiantes:
Estudiante *crearListaEstudiantes()
{
    Estudiante *nuevaListaEstudiantes = NULL; // se inicializa en null
    return nuevaListaEstudiantes;
}

Estudiante *agregarEstudiante(Estudiante *nuevaListaEstudiantes, const char *nombreEstudiante, int edad, int legajo)
{
    Estudiante *nodoNuevo = malloc(sizeof(Estudiante));
    nodoNuevo->nombreEstudiante = malloc(strlen(nombreEstudiante) + 1);
    strcpy(nodoNuevo->nombreEstudiante, nombreEstudiante);
    nodoNuevo->edad = edad;
    nodoNuevo->legajo = legajo;
    nodoNuevo->materiasInscriptas = NULL; // cuando se agrega el estudiante se inicializa en null para que pueda imprimirlo sin tener una materia inscripta
    nodoNuevo->siguiente = NULL;

    if (nuevaListaEstudiantes == NULL) // si la lista está vacia
    {
        nuevaListaEstudiantes = nodoNuevo;
    }
    else // si ya hay un elemento en la lista
    {
        Estudiante *cursor = nuevaListaEstudiantes;
        while (cursor->siguiente != NULL)
        {
            cursor = cursor->siguiente;
        }
        cursor->siguiente = nodoNuevo;
    }
    contadorEstudiantes++; // contador para saber el largo de la lista
    return nuevaListaEstudiantes;
}

Estudiante *generarEstudiantes(Estudiante *nuevaListaEstudiantes, int x)
{
    srand(time(NULL));

    Estudiante *actual = nuevaListaEstudiantes;

    for (int i = 0; i < x; i++)
    {
        // genera 3 iniciales del nombre del alumno
        // me pide tener siempre el ultimo digito de la lista vacio
        char initial[4];

        for (int i = 0; i < 3; i++)
        {
            char c = 65 + rand() % 25;
            initial[i] = c;
        }
        // genera la edad, entre 18 y 60 años
        int age = 18 + rand() % 42;

        int legacy = rand() % 9999;

        actual = agregarEstudiante(actual, initial, age, legacy);
    }

    return actual;
}

void showEstudiantes(Estudiante *l)
{

    Estudiante *actual = l;
    // ancla es la primera iteracion de cada pagina
    // esto es un array de anclas
    Estudiante *anclas;
    anclas = malloc(sizeof(Estudiante) * 100);
    int pos = 0;

    int option;
    //int posicion = 1; faltaria agregar esto
    do
    {
        // cada vez que se haga de nuevo el switch, vuelve a cargarse.
        // las anclas estan antes del primero de cada pagina, en una 'posicion fantasma'
        anclas[pos].siguiente = actual;

        for (int i = 0; i < 25 && actual != NULL; i++)
        {
            printf("Nombre: %s | Edad: %d | Legajo: %04d \n", actual->nombreEstudiante, actual->edad, actual->legajo);
            
            actual = actual->siguiente;
            
        }
        printf("[0 - ver anterior] [1 - ver siguiente] [2 - salir al menu]\n");
        scanf("%d", &option);
        switch (option)
        {
        case 0:
            if (pos > 0)
            {
                pos = pos - 1;
                actual = anclas[pos].siguiente;
            }
            else
            {
                printf("Primera pagina alcanzada\n");
                actual = anclas[pos].siguiente;
            }
            break;
        case 1:
            if (actual == NULL || actual->siguiente == NULL)
            {
                printf("Ultima pagina alcanzada\n");
                actual = anclas[pos].siguiente;
            }
            else
            {
                pos++;
            }
            break;
        default:
            break;
        }
    } while (option != 2);
}

Estudiante *buscarPorNombre(Estudiante *nuevaListaEstudiantes, char *nombreBuscado)
{
    Estudiante *actual = nuevaListaEstudiantes;

    while (actual != NULL)
    {
        if (strcmp(actual->nombreEstudiante, nombreBuscado) == 0)
        {

            return actual;
        }
        actual = actual->siguiente;
    }
    return NULL;
}

// buscar por rango de edad
Estudiante *buscarPorRangoDeEdad(Estudiante *nuevaListaEstudiantes, int edadMin, int edadMax)
{
    Estudiante *estudianteactual = nuevaListaEstudiantes;
    Estudiante *estudiantesEncontrados = crearListaEstudiantes();

    for (estudianteactual = nuevaListaEstudiantes; estudianteactual != NULL; estudianteactual = estudianteactual->siguiente)
    {
        if ((edadMin <= estudianteactual->edad) && (edadMax >= estudianteactual->edad))
        {
            estudiantesEncontrados = agregarEstudiante(estudiantesEncontrados, estudianteactual->nombreEstudiante, estudianteactual->edad, estudianteactual->legajo);
        }
    }
    return estudiantesEncontrados;
}

void anotar(Estudiante *nuevaListaEstudiantes, Materia *nuevaListaMaterias, char *nombreBuscado, const char *materiaBuscada)
{
    Estudiante *estudianteActual = nuevaListaEstudiantes;
    Materia *materiaActual = nuevaListaMaterias;
    int seEncontro = 0; // booleano

    for (materiaActual = nuevaListaMaterias; materiaActual != NULL; materiaActual = materiaActual->siguiente)
    { // pasar por todas las materias

        if (strcmp(materiaActual->nombreMateria, materiaBuscada) == 0)
        { // filtrar materias

            for (estudianteActual = nuevaListaEstudiantes; estudianteActual != NULL; estudianteActual = estudianteActual->siguiente)
            { // pasar por todos los estudiantes

                if (strcmp(estudianteActual->nombreEstudiante, nombreBuscado) == 0) // filtrar estudiantes  (le cambie listaEstudiantes por estudianteActual porque sino no buscaba fuera del primer lugar)
                {
                    estudianteActual->materiasInscriptas = agregarMateria(estudianteActual->materiasInscriptas, materiaActual->nombreMateria, materiaActual->identificador);
                    // estudiante elegido                  guardamos materia en sus materias                    con el nombre de la materia y su codigo

                    materiaActual->estudiantesInscriptos = agregarEstudiante(materiaActual->estudiantesInscriptos, estudianteActual->nombreEstudiante, estudianteActual->edad, estudianteActual->legajo);
                    // materia elegida                     guardamos el estudiante como uno nuevo dentro de sus estudiantes inscriptos

                    printf("se inscribio el estudiante %s a la materia %s\n", nombreBuscado, materiaBuscada);
                    seEncontro = 1;
                    break;
                }
            }
        }
    }
    if (seEncontro == 0)
    { // si no se encontró
        printf("No se encontro el estudiante o la materia\n");
    }
}

int verificarMateriaEstudiante(Nota *nuevaListaNotas, Materia *nuevaListaMaterias, Estudiante *nuevaListaEstudiantes, const char *nombreBuscado, const char *materiaBuscada, int notaParcial1, int notaParcial2, int notaFinal)
{

    Estudiante *estudianteActual = nuevaListaEstudiantes;
    int seEncontro = 0;
    int contadorParaGuardarNota = 0;
    for (estudianteActual = nuevaListaEstudiantes; estudianteActual != NULL; estudianteActual = estudianteActual->siguiente)
    {
        if (strcmp(estudianteActual->nombreEstudiante, nombreBuscado) == 0)
        {
            Materia *materiaActual = estudianteActual->materiasInscriptas;
            for (materiaActual = estudianteActual->materiasInscriptas; materiaActual != NULL; materiaActual = materiaActual->siguiente)
            {
                if (strcmp(materiaActual->nombreMateria, materiaBuscada) == 0)
                {
                    return 1;
                }
            }
        }
    }
    return 0;
}

void showNotasPorEstudiante(Nota *nuevaListaNotas, Estudiante *nuevaListaEstudiantes, char *estudiante)
{
    Nota *nota = nuevaListaNotas;
    Estudiante *estudianteActual = nuevaListaEstudiantes;
    int contador = 0;
    int seEncontro = 0;

    for (estudianteActual = nuevaListaEstudiantes; estudianteActual != NULL; estudianteActual = estudianteActual->siguiente)
    {

        if (strcmp(estudianteActual->nombreEstudiante, estudiante) == 0)
        {
            seEncontro = 1;
            for (nota = nuevaListaNotas; nota != NULL; nota = nota->siguiente)
            {

                if (strcmp(nota->nombreEstudianteDeLaNota, estudiante) == 0)
                {
                    contador++;
                    printf("Notas del estudiante %s:", estudiante);
                    printf("\n(%d)Materia: %s\n", contador, nota->nombreMateria);
                    printf("Parcial1 : %d  |  Parcial 2: %d  |  Final: %d \n\n", nota->notaParcial1, nota->notaParcial2, nota->notaFinal);
                }
            }
        }
    }
    if (seEncontro == 0)
    {
        printf("No se encontró el estudiante\n");
    }
}

void removerEstudiante(Estudiante **lista, const char *nombre)
{
    if (*lista == NULL)
    {
        return;
    }

    if (strcmp((*lista)->nombreEstudiante, nombre) == 0)
    {
        Estudiante *siguiente = (*lista)->siguiente;
        free(*lista);
        *lista = siguiente;
        return;
    }

    Estudiante *actual = *lista;
    while (actual->siguiente != NULL)
    {
        if (strcmp(actual->siguiente->nombreEstudiante, nombre) == 0)
        {
            Estudiante *prox = actual->siguiente->siguiente;
            free(actual->siguiente);
            actual->siguiente = prox;
            return;
        }
        actual = actual->siguiente;
    }
}

Estudiante *agregarPorLegajo(Estudiante *lista, const char *nombreEstudiante, int edad, int legajo)
{

    Estudiante *nodoNuevo = malloc(sizeof(Estudiante));
    nodoNuevo->nombreEstudiante = malloc(strlen(nombreEstudiante) + 1);
    strcpy(nodoNuevo->nombreEstudiante, nombreEstudiante);
    nodoNuevo->edad = edad;
    nodoNuevo->legajo = legajo;
    nodoNuevo->materiasInscriptas = NULL;
    nodoNuevo->siguiente = NULL;
    if (lista == NULL)
    {
        lista = nodoNuevo;
    }
    else
    {
        if (nodoNuevo->legajo < lista->legajo)
        {
            nodoNuevo->siguiente = lista;
            lista = nodoNuevo;
        }
        else
        {
            Estudiante *actual = lista;
            while (actual->siguiente != NULL && actual->siguiente->legajo < nodoNuevo->legajo)
            {
                actual = actual->siguiente;
            }
            nodoNuevo->siguiente = actual->siguiente;
            actual->siguiente = nodoNuevo;
        }
    }
    return lista;
}
void removerTodos(Estudiante **lista)
{
    while (*lista != NULL)
    {
        Estudiante *siguiente = (*lista)->siguiente;
        free(*lista);
        *lista = siguiente;
    }
}