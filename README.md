## Integrantes:
- Joaquin Schanzenbach
- Ayelén Milagros Mercado
- Carlos Fernandez
- Matias Divano

## Consignas implementadas
- Dar de alta y listar estudiantes
    - 1 - Agregar Estudiante
    - 2 - Listar Estudiante
    - 9 - Remover Estudiante
- Buscar Estudiantes por Nombre
    - 3 - Buscar Estudiante por Nombre
- Buscar Estudiantes por rango de edad
    - 4 - Buscar Estudiantes por Rango de Edad
- Dar de Alta y Listar Materias
    - 5 - Agregar Materia
    - 6 - Listar Materias
    - 10 - Remover Materia
- Anotarse en una materia
    - 7 - Anotar en una materia
- Rendir una Materia
    - 8 - Rendir Materia

## Además, como extra incorporamos:
- Paginado
- Generar estudiantes y materias aleatorias de forma masiva
    - 11 - Carga Automatica de Estudiantes
    - 12 - Carga Automatica de Materias
- Mostrar todas las notas de los estudiantes
    - 13 - Imprimir notas
- Buscar todas las notas de un unico estudiante
    - 14 - Buscar nota de un estudiante
- Lista de los legajos ordenados de manera ascendente
    - 15 - Lista de legajos ordenados // no funciona con estudiantes generados automaticamente