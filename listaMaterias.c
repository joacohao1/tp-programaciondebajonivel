#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct structMaterias
{
    char *nombreMateria;
    int identificador;
    struct structMaterias *siguiente;
    struct structEstudiantes *estudiantesInscriptos;
} Materia;

// Contador para saber cuantos materias hay:

int contadorMaterias = 0;

// Crear lista de materias:
Materia *crearListaMaterias()
{
    Materia *nuevaListaMaterias = NULL;
    return nuevaListaMaterias;
}

Materia *agregarMateria(Materia *nuevaListaMaterias, const char *nombreMateria, int identificador)
{
    Materia *nodoNuevoMateria = malloc(sizeof(Materia));
    nodoNuevoMateria->nombreMateria = malloc(strlen(nombreMateria) + 1);     
    strcpy(nodoNuevoMateria->nombreMateria, nombreMateria);
    nodoNuevoMateria->identificador = identificador;
    nodoNuevoMateria->estudiantesInscriptos = NULL;
    nodoNuevoMateria->siguiente = NULL; 
    if (nuevaListaMaterias == NULL)      // si la lista está vacia
    {
        nuevaListaMaterias = nodoNuevoMateria;
    }
    else // si ya hay un elemento en la lista
    {
        Materia *cursor = nuevaListaMaterias;
        while (cursor->siguiente != NULL)
        {
            cursor = cursor->siguiente;
        }
        cursor->siguiente = nodoNuevoMateria;
    }
    contadorMaterias++; // contador para saber el largo de la lista
    return nuevaListaMaterias;
}

void showMaterias(Materia *l){
    Materia *actual = l;
    // ancla es la primera iteracion de cada pagina
    // esto es un array de anclas
    Materia *anclas;
    anclas = malloc(sizeof(Materia) * 100);
    int pos = 0;

    int option;

    do {
        // cada vez que se haga de nuevo el switch, vuelve a cargarse.
        // las anclas estan antes del primero de cada pagina, en una 'posicion fantasma'
        anclas[pos].siguiente = actual;
        
        for(int i = 0; i < 10 && actual != NULL; i++){
            printf("Nombre de materia: %s | identificador: %03d  \n", actual->nombreMateria, actual->identificador);

            actual = actual->siguiente;
        }
        printf("[0 - ver anterior] [1 - ver siguiente] [2 - salir al menu]\n");
        scanf("%d", &option);
        switch (option) {
        case 0:
            if(pos>0){
                pos = pos - 1;
                actual = anclas[pos].siguiente;
            } else {
                printf("Primera pagina alcanzada\n");
                actual = anclas[pos].siguiente;
            }
            break;
        case 1:
            if(actual == NULL || actual->siguiente == NULL){
                printf("Ultima pagina alcanzada\n");
                actual = anclas[pos].siguiente;
            } else {
                pos++;
            }
            break;
        default:
            break;
        }
    } while(option != 2);
}

Materia* generarMaterias(Materia *nuevaListaMaterias, int x){
    time_t t;
    srand( (unsigned) time(&t) );

    Materia* actual = nuevaListaMaterias;

    for(int y = 0 ; y < x ; y++){
        // genera 2 iniciales del nombre del alumno
        // me pide tener siempre el ultimo digito de la lista vacio
        char name[3];

        for (int i = 0; i < 2; i++){
            char c = 65 + rand() % 25;
            name[i] = c;
        }

        int ID = rand() % 999;

        actual = agregarMateria(actual, name, ID);
    }

    return actual;
}

void removerMateria(Materia **listaMateria, const char *nombre)
{
    if (*listaMateria == NULL)
    {
        return;
    }

    if (strcmp((*listaMateria)->nombreMateria, nombre) == 0)
    {
        Materia *siguiente = (*listaMateria)->siguiente;
        free(*listaMateria);
        *listaMateria = siguiente;
        return;
    }

    Materia *actual = *listaMateria;
    while (actual->siguiente != NULL)
    {
        if (strcmp(actual->siguiente->nombreMateria, nombre) == 0)
        {
            Materia *prox = actual->siguiente->siguiente;
            free(actual->siguiente);
            actual->siguiente = prox;
            return;
        }
        actual = actual->siguiente;
    }
}
void removerTodas(Materia **lista)
{
    while (*lista != NULL)
    {
        Materia *siguiente = (*lista)->siguiente;
        free(*lista);
        *lista = siguiente;
    }
}

Materia *buscarMateria(Materia *lista, char *nombreBuscado)
{
    Materia*actual = lista;
    

    while (actual != NULL)
    {
        if (strcmp(actual->nombreMateria, nombreBuscado) == 0)
        {

            return actual;
        }
        actual = actual->siguiente;
        
    }
    return NULL;
}
